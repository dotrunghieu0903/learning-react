# style-guide-react
https://github.com/airbnb/javascript/tree/master/react

# serverless
https://github.com/serverless/examples
https://serverless.com/framework/docs/getting-started/
https://serverless.com/framework/docs/providers/aws/cli-reference/invoke/#invoke-local
https://serverless.com/framework/docs/providers/aws/

# proxy
https://www.jhipster.tech/configuring-a-corporate-proxy/

# webpack
https://github.com/webpack/webpack

# ant-design-control
https://github.com/ant-design/ant-design/tree/master/

# document
* [Get started React](https://reactjs.org/docs/getting-started.html)
* [Main Concepts](https://reactjs.org/docs/hello-world.html)
* [Advanced Guides](https://reactjs.org/docs/jsx-in-depth.html)
* [API Reference](https://reactjs.org/docs/react-api.html)
* [Where to Get Support](https://reactjs.org/community/support.html)
* [Contributing Guide](https://reactjs.org/docs/how-to-contribute.html)
* [Webpack](https://webpack.js.org/guides/getting-started/)
* [Ant Design](https://ant.design/docs/react/getting-started/)
* [The Application Shell](https://angular.io/tutorial/toh-pt0)
* [Displaying a List](https://angular.io/tutorial/toh-pt1)
* [Master/Detail Components](https://angular.io/tutorial/toh-pt3)
* [Lifting State Up](https://reactjs.org/tutorial/tutorial.html#lifting-state-up)
* [Routing Tutorial](https://angular.io/tutorial/toh-pt5)
* [Routing React](https://github.com/ReactTraining/react-router)
* [Routing QuickStart](https://reacttraining.com/react-router/web/guides/quick-start)
* [Routing API history](https://reacttraining.com/react-router/web/api/history)
* [Routing Location](https://reacttraining.com/react-router/web/api/location)
* [Routing Match](https://reacttraining.com/react-router/web/api/match)
* [AJAX and APIS](https://reactjs.org/docs/faq-ajax.html)

# reference
https://github.com/99xt/serverless-react-boilerplate
https://reacttraining.com/react-router/web/guides/quick-start

# vs code extension
ES7 React/Redux/GraphQL/React-Native snippets
GitLens
Prettier - Code formatter
vscode-icons