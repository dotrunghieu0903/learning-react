import { Label, Form, Input, Button } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import '../Login/Login.css'

function Login() {
  return (
    <div className="div-login">
      <Form className="form-login">
        <div className="ui label">
          <Label>UserName</Label>
        </div>
        <div className="ui input">
          <Input type="text"></Input>
        </div>

        <div className="ui label">
          <Label>Password</Label>
        </div>
        <div className="ui input">
          <Input type="password"></Input>
        </div>
        <div>
        <Button>Login</Button>
        <Button>Cancel</Button>
        </div>
      </Form>

    </div>
  );
}

export default Login;
